---
Week: 6
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 6 to 9 Network Destination NAT

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* Destination NAT.
* Port Forwarding.
* Troubleshooting  

### Topics to be covered

* Destination NAT.
* Port Forwarding.
* Troubleshooting.

## Deliverables

* Work on Virtual SRX and Virtual Raspberries.
* Assignment 33 SRX destination nat
* Demonstrate to the lecturer when it works.

## Schedule

Class B Friday/Class A Wednesday

## Hands-on time

* See deliverables.

## Comments

TBD

## Software

Use Putty to access the routers.

## Sources

* 19S Junos destination NAT PDA Vxx.pdf

* SRX security configurations. Zones, Policies and Firewall. 
https://gitlab.com/PerPer/networking/-/tree/master/SRX%20security

* 18A VMware Workstation 2 SRX routers DHCP PDA V04.pdf  
Find it on ITSL in Resources

* 18A VMware Workstation SRX BRIDGE NAT Internet PDA V06.pdf

* Troubleshooting  
https://gitlab.com/PerPer/networking/blob/master/Trouble%20shooting/Network_Troubleshooting_Per_Dahlstroem_UCL.md

## White Board

TBD

