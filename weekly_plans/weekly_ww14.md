---
Week: 14
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 14 - 15 (Easter) - 16 Network Radius server

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* What AAA is.
* What a RADIUS server is27. (Remote Authentication Dial In User Service).
* Benefits of and when to use a Network Access Server NAS/Remote Access Server RAS.
* Optional: The use of a Relational Database with Radius authentication server

The student can:  

* Set up a FreeRADIUS server on Linux and configure it for authentication.
* Set up e.g. a SRX240 for user authentication from a Radius server.
* Optional: Set up a Relational Database with Radius authentication server.
* Optional: Set up e.g. a SRX240 for 802.1X user authentication from a Radius server.

### Topics to be covered

* Radius Authentication, Authorization and Accounting. AAA
* Optional: 802.1x

## Deliverables

* Assignment 41 RADIUS AAA server. (Will be edited and ready 31 March!)
* https://gitlab.com/PerPer/networking/-/tree/master/Networking_Assignments

## Schedule

Class A Wednesday 08:15 - 15:30 and Class B Friday 08:15 - 15:30.
Due to Corona the schedule might change. Please consult TimeEdit.
Check on TimeEdit too.

## Hands-on time

* See deliverables.

## Comments

TBD

## Software

Use Putty to access the routers.

## Sources

* 19S_FreeRADIUS_PDA_Vxx.pdf

* Juniper_SRX_Branch_Router_Switching.pdf

* Troubleshooting  
https://gitlab.com/PerPer/networking/blob/master/Trouble%20shooting/Network_Troubleshooting_Per_Dahlstroem_UCL.md

Videos from the class RADIUS session Wednesday: 01_04_20

Part_1: The Radius basics.

* https://drive.google.com/file/d/1iesHgvdbkRXjUIA3s0H0CWoEHLqL3Mq2/view  

Part_2: The Radius basics.

* https://drive.google.com/file/d/1Jf03ld54K-sZx4ggNu25xPaBCPkNt_sQ/view  

Part_3: The Radius basics.

* https://drive.google.com/file/d/1g_ThfFCW016_k_K4HL0eDN-PS6e6jhSl/view

Part_4: Junos 802.1x only for physical devices not implemented on the vSRX:

* https://drive.google.com/file/d/1g_ThfFCW016_k_K4HL0eDN-PS6e6jhSl/view

Part_5: Walking through the step-by-step document:

* https://drive.google.com/file/d/1QITD8kN3UwnM7aET9mG5eOW0Vs5gZqV6/view

Videos from the class RADIUS and SQLite (Optional) session Friday: 17_04_20

Part_1: The SQLite basics. (3:45)

* https://drive.google.com/file/d/1TaerBZCnpfPqfG1eUNYsJ2W3kbyYZ9sn/view

Part_2: The SQLite basics. SQLite versus mySQL (3:10)

* https://drive.google.com/file/d/17JwWPLvCxUbAgaWl03fAN964U-KglufQ/view

Part_3: The SQLite basics. DB-browser and user administration. (3:45)

* https://drive.google.com/file/d/1pLouIBz4leTF7CagmHI_44jQ2J5hxKyW/view

## White Board

TBD

