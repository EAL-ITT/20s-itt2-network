---
Week: 5
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 5 Network DHCP on Junos

## Goals of the week(s)
Pratical and learning goals for the period are as follows

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* DHCP.
* DHCP on Junos.
* Troubleshooting  

### Topics to be covered

* DHCP.
* DHCP on Junos physical device.
* Troubleshooting.

## Deliverables

* Show the network diagram to the lecturer.
* Demonstrate to the lecturer that the DHCP is working on the physical Juniper device.

## Schedule

Class B Friday/Class A Wednesday

## Hands-on time

* See deliverables.

## Comments

TBD

## Software

Use Putty to access the routers.

## Sources

* 18A VMware Workstation 2 SRX routers DHCP PDA V04.pdf  
Find it on ITSL in Resources

* A video on the DHCP concept  
https://www.youtube.com/watch?v=e6-TaH5bkjo

* Troubleshooting  
https://gitlab.com/PerPer/networking/blob/master/Trouble%20shooting/Network_Troubleshooting_Per_Dahlstroem_UCL.md

## White Board

TBD

