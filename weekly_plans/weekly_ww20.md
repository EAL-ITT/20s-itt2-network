---
Week: 20
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 20 - 21 Network SAMBA file server

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* Samba file server.
* The purpose of SMB.

### Topics to be covered

* Samba file server.
* To some extend the SMB protocol. 

## Deliverables

* Assignment 47 Samba file server
* Also demonstrate to the lecturer when files can be shared and document.

## Schedule

Check on TimeEdit.

## Hands-on time

* See deliverables.

## Comments

TBD

## Software

Use Putty to access the routers.

## Sources

* 20S_Samba_File_Server_on_Raspberry_PDA_Vxx.pdf  
https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/20S_2_semester_network_literature/SAMBA_file_server  

* PDA video on SAMBA:  
https://drive.google.com/file/d/1B6m4_OfxhaVyT-DTVoFiCSISrp0wRjCA/view  

* A very brief high level explanation of the protocols used by SAMB and why SAMBA should be known by IoT networking professionals. Why should it?  
https://www.youtube.com/watch?v=UhvuApzNZO  
(Only watch until: 02:37. The rest is for VirtualBox. We use VMWW)  

* A detailed SAMBA installation via GUI on Ubuntu. No high level explanation but good material on the security side of using SAMBA:  
https://www.youtube.com/watch?v=-wUfzdiE4m8

* Troubleshooting  
https://gitlab.com/PerPer/networking/blob/master/Trouble%20shooting/Network_Troubleshooting_Per_Dahlstroem_UCL.md

## White Board

TBD

