---
Week: 22
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 22 - 23 - 24 Network

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* OSPF  

### Topics to be covered

* OSPF
* Please note that we will also do review questions in week 24.

## Deliverables

* Assignment 49 or 50 or 51. Optionally 52.  
https://gitlab.com/PerPer/networking/-/tree/master/Networking_Assignments

## Schedule

Check on TimeEdit.

## Hands-on time

* See deliverables.

## Comments

TBD

## Software

Use Putty to access the routers.

## Sources

* Example configurations for assignments(s) can be found on GitLab:  
https://gitlab.com/PerPer/networking/-/tree/master/SRX%20configurations/OSPF

* An OSPF video by PDA:  
https://media.videotool.dk?vn=277_2020052911143290370402011017

* A video explaining the mechanics of OSPF. Not about configuring OSPF.  
For configuration, the video explains the concepts, router ID and  Designatet Router DR and Backup Designatet Router BDR. It also explains e.g. the concept OSPF Neighbours and the Neighbourg states: Init and Full OSPF neighbourgh. These concepts will show up in configurations and debugging.  
https://www.youtube.com/watch?v=kfvJ8QVJscc  

* A Juniper student guide for routing. Only pages: 1-14 to 1-16 and 2-17 to 2-10. Of course students are free to study the entire chapters.   
JNCIA-JRE-Junosphere-12.c-R_SG.pdf  
https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/20S_2_semester_network_literature  


* Troubleshooting  
https://gitlab.com/PerPer/networking/blob/master/Trouble%20shooting/Network_Troubleshooting_Per_Dahlstroem_UCL.md

## White Board

TBD

