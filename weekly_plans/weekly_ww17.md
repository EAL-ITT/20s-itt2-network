---
Week: 17
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 17 - 18 Network NTP server

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* NTP.
* Troubleshooting  

### Topics to be covered

* NTP
* Troubleshooting.

## Deliverables

* Assignment 47 NTP server  
* Demonstrate to the lecturer when it works.

## Schedule

Check on TimeEdit. Due to Corona the schedule is adjusted compared with the outset.

## Hands-on time

* See deliverables.

## Comments

TBD

## Software

Use Putty to access the routers.

## Sources

* 19S NTP server on Linux PDA Vxx.pdf  
Find it here: https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature

* Troubleshooting  
https://gitlab.com/PerPer/networking/blob/master/Trouble%20shooting/Network_Troubleshooting_Per_Dahlstroem_UCL.md


## Video from the class on NTP Wednesday: 22_04_20  

Part_1: The NTP basics and a high level walk through. (10:18)

* https://media.videotool.dk?vn=277_2020050710490620109388150067  
* https://drive.google.com/file/d/1B3VkaeBQ2ysOseMXGvH2_JtL8g8jxmn8/view 

Part_2: There is no part two.

## White Board

TBD

