---
Week: 10
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 10 and 11 Network SSH client and server

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* Backup and recovery plan and restore for a defined network system.
* SSH client and server.

### Topics to be covered

* SSH client and server.
* Backup and recovery plan and restore for a defined network system.

## Deliverables

* Work in VMware Workstation. I.e. virtualised.
* Assignment TBD.
* Hand in on Peergrade.

## Schedule

Class B Friday/Class A Wednesday

## Hands-on time

* See deliverables.

## Comments

TBD

## Software

Use Putty to access the routers.

## Sources

* Network_SSH_Per_Dahlstroem_UCL  
https://gitlab.com/PerPer/networking/blob/master/SSH/Network_SSH_Per_Dahlstroem_UCL%20.md

* Troubleshooting  
https://gitlab.com/PerPer/networking/blob/master/Trouble%20shooting/Network_Troubleshooting_Per_Dahlstroem_UCL.md

## White Board

TBD

