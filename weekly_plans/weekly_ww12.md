---
Week: 12 - 13
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 12 - 13 Network DNS server

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* DNS
* Troubleshooting  

### Topics to be covered

* DNS. How to set up a local DNS server.

## Deliverables

* Assignment 42 DNS Server
* Hand in on PeerGrade.

## Schedule

Class A Wednesday 08:15 - 15:30 and Class B Friday 08:15 - 15:30.
Check on TimeEdit too.

## Hands-on time

* See deliverables.

## Comments

Please on the Network lecture day at 09:00 join the Virtual classromm: https://meet.google.com/kfr-prpe-itu

## Software

Use Putty to access routers.

## Sources

* https://gitlab.com/PerPer/networking/-/tree/master/pda_instructions%2FNetworking  
Please see the document on DNS.
* Troubleshooting  
https://gitlab.com/PerPer/networking/blob/master/Trouble%20shooting/Network_Troubleshooting_Per_Dahlstroem_UCL.md  

Videos from the class A session: 20_03_18

* https://drive.google.com/file/d/12FwsDXkF2nR98DH-0yJe-siP0kAJUz4o/view?usp=sharing_erl  

* https://drive.google.com/file/d/1JyL4tizT80mDLTs6-tisbMT3ig5XI3iq/view?usp=sharing_erl  

Videos from the class B session: 20_03_20

Part_1:

* https://drive.google.com/open?id=1pRMcHaTNb9A2dqGXSJ3FXWeRTh4TgOF3  

Part_2:

* https://drive.google.com/open?id=1lIjrXl4Zq2CPdrZ_yrSLZq2Y-jzxy_mf  

Part_3:

* https://drive.google.com/open?id=1Qg22TU6tG-cMf8JnQVK7Exv6L-vfMNy5

## White Board

TBD

