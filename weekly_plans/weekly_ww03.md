---
Week: 3
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 3 Networking Review questions

## Goals of the week
Pratical and learning goals for the period are as follows

### Practical goals

* Review questions.
* Multiple Choice exam. See Semester Descriptionn on ucl.dk.

### Learning goals

* Review questions.
* Multiple Choice exam. See Semester Descriptionn on ucl.dk. 

### Topics to be covered

* Review questions. Students ask question on topics where they feel insecure.

## Deliverables

None.

## Schedule

Class B Tuesday/Class A Wednesday

## Hands-on time

* Review questions.

## Comments

TBD

## Software

Use Putty to access the routers.

## Sources

* See all previous Weekly plans ressources.

## White Board

TBD

