---
title: '20S ITT2 networking'
subtitle: 'Lecture plan'
authors: ['Per Dahlstrøm \<pda@ucl.dk\>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait19, 19A+B
* Name of lecturer and date of filling in form: PDA 2020-01-07
* Title of the course, module or project and ECTS: Network, 6 ECTS
* Required readings, literature or technical instructions and other background material: Material will be specified in the weekly plans.

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


-------- ------ ---------------------------------------------
 INIT     Week  Content
-------- ------ ---------------------------------------------
PDA       05    DHCP on physical devices.

PDA       06    Destination NAT walkthrough and work on class
              
PDA       07    Destination NAT walkthrough and work on class

PDA       08    Destination NAT walkthrough and work on class

PDA       09    Destination NAT walkthrough and work on class
                
PDA       10    Diesaster backup and restore. SSH server and client

PDA       11    Disaster backup and restore. SSH server and client

PDA       12    DNS server. OLA.
                
PDA       13    DNS server. OLA.

PDA       14    Radius authentication server.

PDA       15    Easter break.
                
PDA       16    Radius authentication server. Optional: SQLite combo.

PDA       17    NTP Server.

PDA       18    NTP Server.

PDA       19    No lectures due to Big prayer day.

PDA       20    Linux file server. Samba.

PDA       21    Linux file server. Samba.

PDA       22    OSPF

PDA       23    OSPF

PDA       24    OSPF and Review

PDA       25    No lectures

PDA       26    Exam
                
PDA       26    Semester ends

                
---------------------------------------------------------


# General info about the course, module or project

## The student’s learning outcome

Please see the Curriculum2018 section 2. 2.1.

## Content

Please see the weekly plans for detailed content of the course. Also consult the overalle topic by weeks above here.

## Method

The Networking part of the education is a mix of lectures, assignments and practical work on both virtualized and pysical physical network ecosystems in the lab.
During tghe course there is a number of hand ins and peer reviews. I.e. students get mutual inspiration from each other and facilitates their mutual learning processes.

## Equipment

The student is expectd to have a Laptop strong enough to run the newest version of VMware Workstation.

## Projects with external collaborators  (proportion of students who participated)

The subject Project integrates external collaborators and Networking.

## Test form/assessment
The course includes a number of obligatory elements.

See semester plan/description for details on obligatory elements.

## Other general information
None at this time.