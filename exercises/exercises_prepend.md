---
title: 'network'
subtitle: 'exercises'
authors: ['Per Dahlstrøm \<pda@ucl.dk\>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'network, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans. 

References
